#include "task.h"

Task::~Task()
{

}
Task::Task()
{
    rightAnswer = 0;
    question = "";
    difficulty = 1;
    theme = "";
}
Task::Task(QString _question, QList<QString> _answers, int _rightAnswer, QString _difficulty, QString _theme)
{
    question = _question;
    answers = _answers;
    rightAnswer = _rightAnswer;
    if(_difficulty == "easy")
        difficulty = 1;
    else if(_difficulty == "medium")
        difficulty = 2;
    else difficulty = 3;

    theme = _theme;
}
QString Task::getQuestion() const
{
    return question;
}
QList<QString> Task::getAnswers() const
{
    return answers;
}
int Task::getRightAnswer() const
{
    return rightAnswer;
}
int Task::getDifficulty() const
{
    return difficulty;
}
QString Task::getTheme() const
{
    return theme;
}
