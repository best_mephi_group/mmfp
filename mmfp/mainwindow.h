#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDomDocument>
#include <QVBoxLayout>
#include <QList>
#include <QRadioButton>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QProgressBar>
#include "task.h"
#include "authdialog.h"

class authdialog;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void prepareTestGui();
    void buildTest();
    void shuffleTasks(QList<Task> tasksList, int COUNT);
    void hideTestGui();
    void createTaskGui();
    int getAnswerId();
    QString createHtml();

    void setAllTasks(QList<Task> tasks);
    QList<Task> getAllTasks();
    void confTextUpdate();    
    void setStudentName(QString studentName);

private slots:
    //pages buttons
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();

    //tests page buttons
    void prevTaskLoad();
    void nextTaskLoad();
    void checkAnswerCorrectness();

    //configuration page buttons
    void on_addTaskButton_clicked();
    void on_saveButton_clicked();
    void on_editTaskButton_clicked();
    void on_deleteTaskButton_clicked();

public slots:
    void setSecretChapterIsVisible();
    void startTest();
    void openHelp();

    void on_pushButton_5_clicked();

private:
    Ui::MainWindow *ui;
    authdialog *auth_dialog;
    bool authorized = false;
    bool isTestStarted = false;
    QString m_studentName;

    //for tests
    QDomDocument testsDom;
    QList<Task> m_tasks;//10 for current test
    QList<Task> m_easyTasks;
    QList<Task> m_mediumTasks;
    QList<Task> m_difficultTasks;
    int currentTask = 0;

    //for tests GUI
    QList<QRadioButton*> m_answersList;
    QLabel *m_questLabel;
    QLineEdit *m_lineAnswer;
    QPushButton *m_prevButton;
    QProgressBar *m_completenessBar;
    QPushButton *m_nextButton;
    QMap<int, int> m_correctness;
    QMap<int, int> m_selectedAnswers;

    //for conf
    QList<QString> m_themes;
    QList<Task> m_alltasks;

    QString m_log = "";

    void setPage(int index);
    void updateThemes();
    quint32 key = 073; //шифр
    QString encodeStr(const QString& str);
    QString decodeStr(const QString &str);
    QString getCurrentTimeF();

};

#endif // MAINWINDOW_H
