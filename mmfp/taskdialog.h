#ifndef TASKDIALOG_H
#define TASKDIALOG_H

#include <QDialog>
#include <QString>
#include <QList>
#include <task.h>
#include "mainwindow.h"

namespace Ui {
class TaskDialog;
}

class TaskDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TaskDialog(QWidget *parent = 0);
    ~TaskDialog();

    void setParams(QList<QString> themes, MainWindow *_main);
    bool isEdit = false;
    bool isDelete = false;

private slots:
   void on_addButton_clicked();
   void on_themecomboBox_currentTextChanged(const QString &arg1);
   void on_comboBox_currentIndexChanged(int index);

private:
    Ui::TaskDialog *ui;
    QList<QString> m_themes;
    QList<Task> m_tasks;
    MainWindow *m_main;

    void displayTask(int index);


};

#endif // TASKDIALOG_H
