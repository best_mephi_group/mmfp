#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "task.h"
#include "taskdialog.h"
#include "resultsdialog.h"
#include "qcustomplot.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDomDocument>
#include <QList>
#include <QRadioButton>
#include <QGroupBox>
#include <QDebug>
#include <QProgressBar>
#include <QTime>
#include <QTextCodec>
#include <iostream>
#include <fstream>
#include <cmath>
#include <QGraphicsScene>
#include <QGraphicsView>

using namespace std;

int ANS_COUNT = 5; //максимальное число вариантов
int TASK_COUNT = 10; // всего вопросов в тесте

QString MainWindow::getCurrentTimeF()
{
    return  QString("[" + QDateTime().currentDateTime().toString() + "]");
}

QList<int> getRandomIntVector(int start, int countOfNumbers, int length)
{
    qsrand (QDateTime::currentMSecsSinceEpoch());
    QList<int> numbers;
    QList<int> returnNumbers;
    for (int i = start; i < countOfNumbers; i++)
        numbers.append(i);
    for (int i = 0; i < length; i++)
    {
        int num = qrand() % numbers.count();
        returnNumbers.append(numbers.at(num));
        numbers.removeAt(num);
    }
    return returnNumbers;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFont buttonFont = QFont("Times", 16, QFont::Bold);
    QSize buttonSize = QSize(50,50);


    ui->pushButton->setFont(buttonFont);
    ui->pushButton->setIcon(QIcon("://icons/lec.png"));
    ui->pushButton->setIconSize(buttonSize);
    ui->pushButton_2->setFont(buttonFont);
    ui->pushButton_2->setIcon(QIcon("://icons/mod.png"));
    ui->pushButton_2->setIconSize(buttonSize);
    ui->pushButton_3->setFont(buttonFont);
    ui->pushButton_3->setIcon(QIcon("://icons/die.png"));
    ui->pushButton_3->setIconSize(buttonSize);
    ui->pushButton_4->setFont(buttonFont);
    ui->pushButton_4->setIcon(QIcon("://icons/conf.png"));
    ui->pushButton_4->setIconSize(buttonSize);

    connect (ui->helpAction, SIGNAL(triggered()), this, SLOT(openHelp()));
    this->setWindowState(Qt::WindowMaximized);

    QFile file("://lectures/test.html");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Error Lectures", file.errorString());
    }
    else
    {
        QString html(file.readAll());
    }
    QGraphicsPixmapItem *logo = new QGraphicsPixmapItem();
    QGraphicsScene *logo_scene = new QGraphicsScene();
    QPixmap logo_bmp;

    if(!logo_bmp.load("://lectures/big.png", "PNG"));
    logo->setPixmap(logo_bmp);
    logo_scene->addItem(logo);

    logo_scene->addItem(logo);

    ui->graphicsView->setScene(logo_scene);
    ui->stackedWidget->setCurrentWidget(ui->page);

    prepareTestGui();
}

void MainWindow::openHelp()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(QApplication::applicationDirPath()+"/help.pdf"));
}

//создает гуи для тестов
void MainWindow::prepareTestGui()
{
    QFont reg_button_font = QFont("Times", 12);
    ui->taskPlainText->setReadOnly(true);
    ui->taskPlainText->zoomIn(2);
    //вопрос
//    m_questLabel = new QLabel("Вопрос");
//    m_questLabel->hide();
//    ui->vanswersLayout->addWidget(m_questLabel);

    //ответы
    for(int i = 0; i < ANS_COUNT; i++)
    {
        QRadioButton* answer = new QRadioButton();
        QHBoxLayout *anshLayout = new QHBoxLayout();
        ui->vanswersLayout->addLayout(anshLayout);
        anshLayout->addStretch();
        anshLayout->addWidget(answer);
        anshLayout->addStretch();
        answer->hide();
        m_answersList.append(answer);
        connect(answer, SIGNAL(clicked()), SLOT(checkAnswerCorrectness()));
    }

    //ответ в текстовую строку
    m_lineAnswer = new QLineEdit();
    m_lineAnswer->hide();
    ui->hansLineLayout->addWidget(m_lineAnswer);
    QSpacerItem *vSpacer = new QSpacerItem(200, 0);
    ui->hansLineLayout->addSpacerItem(vSpacer);

    //кнопка назад
    m_prevButton = new QPushButton();
    m_prevButton ->setFont(reg_button_font);
    m_prevButton->setText("Предыдущий");
    m_prevButton->setDisabled(true);
    ui->hcontrolLayout->addWidget(m_prevButton);
    connect(m_prevButton, SIGNAL(clicked()), SLOT(prevTaskLoad()));

    //прогресс бар
    ui->hcontrolLayout->addStretch();
    m_completenessBar = new QProgressBar();
    m_completenessBar->setMaximum(TASK_COUNT);
    m_completenessBar->setMinimum(0);
    m_completenessBar->setValue(0);
    m_completenessBar->setTextVisible(true);
    m_completenessBar->setAlignment(Qt::AlignCenter);
    m_completenessBar->setFormat(QString("Ответов %1/%2").arg(m_completenessBar->value()).arg(TASK_COUNT));
    ui->hcontrolLayout->addWidget(m_completenessBar);
    ui->hcontrolLayout->addStretch();

    //кнопка вперед
    m_nextButton = new QPushButton();
    m_nextButton ->setFont(reg_button_font);
    m_nextButton->setText("Следующий");
    m_nextButton->setDisabled(true);
    ui->hcontrolLayout->addWidget(m_nextButton);
    connect(m_nextButton, SIGNAL(clicked()), SLOT(nextTaskLoad()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::setPage(int index)
{
    if(ui->stackedWidget->currentIndex() == 3)
    {
        QMessageBox mb(0 , tr("Все несохраненные изменения \nбудут потеряны. Продолжить?"),
                             QMessageBox::Warning,
                             QMessageBox::Yes,
                             QMessageBox::No | QMessageBox::Default | QMessageBox::Escape ,
                             QMessageBox::NoButton  );
            mb.setButtonText(QMessageBox::Yes, tr("Да"));
            mb.setButtonText(QMessageBox::No, tr("Отмена"));
            QPixmap exportSuccess("://icons/main.png");
            mb.setWindowIcon(QIcon(exportSuccess));
            if( mb.exec() == QMessageBox::Yes ) {

            } else {
              return;
            }
    }
    if (ui->stackedWidget->currentIndex() == 2 && isTestStarted && index != 4)
    {
        return;
    }
    qDebug() << ui->stackedWidget->currentIndex();
    qDebug() << index;

    switch (index) {
    case 1:
        ui->stackedWidget->setCurrentWidget(ui->page);
        break;
    case 2:
        ui->stackedWidget->setCurrentWidget(ui->page_2);
        break;
    case 3:
        if(!isTestStarted)
        {
            auth_dialog = new authdialog(this);
            auth_dialog->setStatusTest(this);
            connect(auth_dialog, &authdialog::showTest, this, &MainWindow::startTest);
            auth_dialog->show();
        }
        else ui->stackedWidget->setCurrentWidget(ui->page_3);
        break;
    case 4:
        ui->stackedWidget->setCurrentWidget(ui->page_4);
        isTestStarted = false;
        break;
    default:
        return;
        break;
    }
}

void MainWindow::startTest()
{
    isTestStarted = true;
    setPage(3);
}

void MainWindow::setStudentName(QString studentName)
{
    m_studentName = studentName;
    m_log += getCurrentTimeF() + "Начало тестирования. Студент " + m_studentName + "\r\n";
}

void MainWindow::on_pushButton_clicked()
{
    authorized = false;
    setPage(1);

    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
}

void MainWindow::on_pushButton_2_clicked()
{
    authorized = false;
    setPage(2);

    ui->stackedWidget->setCurrentWidget(ui->page_2);
    //ui->lineEdit->insert(QString::number(1));
    ui->lineEdit_2->insert(QString::number(0.003));
    ui->lineEdit_3->insert(QString::number(0.001));
    ui->lineEdit_4->insert(QString::number(1000));
    ui->lineEdit_5->insert(QString::number(0.01));
    ui->doubleSpinBox->setValue(0);
    ui->doubleSpinBox_2->setValue(10);
    ui->doubleSpinBox_3->setValue(0);
    ui->doubleSpinBox_4->setValue(6);
}



void MainWindow::on_pushButton_3_clicked()
{
    authorized = false;
    if(isTestStarted)
        return;
    //открываем файл с тестами и обрабатываем
    QFile file("lib/my.tests");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Невозможно открыть файл с тестами", file.errorString());
        return;
    }    
    else
    {
        QString reads =  decodeStr(QString(file.readAll()));
        qDebug() << reads;
        testsDom.setContent(reads);
        file.close();
    }

    currentTask = 0;
    buildTest();
    createTaskGui();

    setPage(3);

    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
}

void MainWindow::on_pushButton_4_clicked()
{
    if (authorized || ui->stackedWidget->currentIndex() == 3){
        setSecretChapterIsVisible();
    }
    else{
        auth_dialog = new authdialog(this);
        connect(auth_dialog, &authdialog::showSecretChapter, this, &MainWindow::setSecretChapterIsVisible);
        auth_dialog->show();
    }
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
}


void MainWindow::buildTest()
{

    QString question;
    QList<QString> answers;
    int rightAnswer;
    QString difficulty;
    m_easyTasks.clear();
    m_mediumTasks.clear();
    m_difficultTasks.clear();
    m_themes.clear();
    m_tasks.clear();
    m_correctness.clear();
    m_selectedAnswers.clear();
    m_completenessBar->setValue(0);
    m_completenessBar->setFormat(QString("Ответов %1/%2").arg(m_completenessBar->value()).arg(TASK_COUNT));

    QDomNodeList themesList = testsDom.elementsByTagName("theme");
    QDomNode themeNode;
    QString theme;
    QDomNodeList partsList;
    for (int i = 0; i < themesList.count(); i++)
    {

       theme = themesList.at(i).toElement().attribute("name");
       partsList = themesList.at(i).toElement().elementsByTagName("part");

       for (int j = 0; j < partsList.count(); j++)
       {
           answers.clear();
           QDomElement elem = partsList.at(j).toElement();
           QDomNodeList qAa = elem.childNodes();
           question = qAa.at(0).toElement().text();
           qDebug() << question << " " << qAa.count();
           for (int k = 1; k < qAa.count(); k++)//because first was answer
           {
               if (qAa.at(k).toElement().attribute("correct").contains("true"))
                   rightAnswer = k-1;
               answers.append(qAa.at(k).toElement().text());
           }
           difficulty = elem.attribute("difficulty");

           QList<int> _randNumbers = getRandomIntVector(0, answers.count(), answers.count());
           rightAnswer = _randNumbers.indexOf(rightAnswer);
           qDebug() << _randNumbers;
           QList<QString> _buf = answers;
           answers.clear();//before shuffling
           for(int ind = 0; ind < _buf.count(); ind++)
           {
               answers.append(_buf.at(_randNumbers.at(ind)));
           }

           Task newTask(question, answers, rightAnswer, difficulty, theme);
           if(difficulty == "easy")
           {
               m_easyTasks.append(newTask);
           }
           else if( difficulty == "medium")
           {
               m_mediumTasks.append(newTask);
           }
           else
           {
               m_difficultTasks.append(newTask);
           }
       }
    }
    qDebug() << "Успешно распарсили";

    shuffleTasks(m_mediumTasks, 4);
    shuffleTasks(m_difficultTasks, 2);
    shuffleTasks(m_easyTasks, 4);
    qDebug() << "Успешно расшафлили";

    if(m_mediumTasks.count() < 4 || m_easyTasks.count() < 4 || m_difficultTasks.count() < 2)
    {
        QMessageBox::information(this ,"Внимание", "Количество заданий недостаточно для формирования теста\n (нужно 4 легких, 4 средний, 2 тяжелых)");
        return;
    }

    QList<int> randNumbers = getRandomIntVector(0, TASK_COUNT, TASK_COUNT);
    QList<Task> buf = m_tasks;
    m_tasks.clear();//before shuffling
    for(int i = 0; i < TASK_COUNT; i++)
    {
        m_tasks.append(buf.at(randNumbers.at(i)));
    }

}

void MainWindow::shuffleTasks(QList<Task> tasksList, int COUNT)
{
    int count = tasksList.count();
    QList<int> randNumbers;
    if(count < COUNT)
    {
         randNumbers  = getRandomIntVector(0, count, count);
    }
    else
    {
        randNumbers  = getRandomIntVector(0, count, COUNT);
    }
    for(int i = 0; i < randNumbers.count(); i++)
    {
        m_tasks.append(tasksList.at(randNumbers.at(i)));
    }
}

//скрывание всех элементов гуи для обновления к следующему заданию
void MainWindow::hideTestGui()
{
    ui->taskPlainText->clear();
    for(int i = 0; i < ANS_COUNT; i++)
    {
        m_answersList.at(i)->setChecked(true);
        m_answersList.at(i)->hide();
    }
    //select previus answers if need
    if(m_selectedAnswers.contains(currentTask)){
        m_answersList.at(m_selectedAnswers[currentTask])->setChecked(true);
    }

    m_lineAnswer->hide();
    m_prevButton->setDisabled(true);
    m_nextButton->setDisabled(true);
}

QString MainWindow::createHtml()
{
    QString str;
    str.append(QString("<center><h2>Вопрос №") + QString::number(currentTask+1) + QString("</center></h2>"));
    str.append(QString("<p><i>") + m_tasks.at(currentTask).getQuestion() + QString("</i></p>"));
    QList<QString> answers = m_tasks.at(currentTask).getAnswers();
     for (int i = 0; i < answers.count(); i++)
     {
         str.append(QString("<p>") + QString::number(i+1) + QString(". ") + answers.at(i) + QString("</p>"));
     }
     return str;
}
//показ гуи для текущего вопроса
void MainWindow::createTaskGui()
{
    hideTestGui();
    QString htmlOutput = createHtml();
    ui->taskPlainText->setText(htmlOutput);
    //m_questLabel->setText(m_tasks.at(currentTask).getQuestion());

    QList<QString> answers = m_tasks.at(currentTask).getAnswers();
    for (int i = 0; i < answers.count(); i++)
    {
        if (i == ANS_COUNT) break;
        m_answersList.at(i)->setText(QString::number(i+1));
        m_answersList.at(i)->show();
    }

//получение номера выбранного ответа
int MainWindow::getAnswerId()
{

    if(currentTask == m_tasks.count()-1)
    {
        m_nextButton->setDisabled(false);
        m_prevButton->setDisabled(false);
        m_prevButton->setText("Предыдущий");
        m_nextButton->setText("Завершить");
    }
    else if(currentTask == 0)
    {
        m_nextButton->setDisabled(false);
        m_nextButton->setText("Следующий");
    }
    else
    {
        m_nextButton->setDisabled(false);
        m_prevButton->setDisabled(false);
        m_prevButton->setText("Предыдущий");
        m_nextButton->setText("Следующий");
    }
    if(currentTask == 0)
        m_prevButton->setDisabled(true);
}

//получение номера выбранного ответа
int MainWindow::getAnswerId()
{

    for(int i = 0; i < m_answersList.count(); i++)
    {
        if(m_answersList.at(i)->isChecked() && !m_answersList.at(i)->isHidden())
            return i;
    }
    return -1;
}

//загрузка предыдущего вопроса
void MainWindow::prevTaskLoad()
{
    if(currentTask > 0)
        currentTask--;
    createTaskGui();
}

//загрузка следующего вопроса/завершение теста
void MainWindow::nextTaskLoad()
{
    if(getAnswerId() != -1)
        checkAnswerCorrectness();

    QList<int> unanswered = m_correctness.keys();
    if(currentTask == m_tasks.count()-1)
    {
       if(unanswered.count() < m_tasks.count())
       {
           QMessageBox::information(this, "Информация",
                                    QString("%1 unanswered questions").arg(m_tasks.count() - unanswered.count()));
       }
       else
       {
             m_log += getCurrentTimeF() + "Конец тестирования\r\n ******** \r\n";
             QFile fileOut("log.txt");
             if(fileOut.open(QIODevice::Append))
             {
                  QTextStream writeStream(&fileOut);
                  writeStream.setCodec(QTextCodec::codecForName("UTF-8"));
                  writeStream << m_log.toUtf8() ;
                  fileOut.close();
             }

             ResultsDialog *rDialog = new ResultsDialog(this);
             rDialog->setParams(m_studentName, m_correctness, m_selectedAnswers, m_tasks);
             rDialog->show();
             isTestStarted = false;
             setPage(1);
       }
    }
    else
    {        
        currentTask++;
        createTaskGui();
    }
}

//проверка правильности ответа
void MainWindow::checkAnswerCorrectness()
{    
    int id = getAnswerId();
    if (id < 0) return;

    m_log += getCurrentTimeF() + "Выбран ответ " + QString("№%1").arg(id+1) + " на вопрос " + QString("#%1").arg(currentTask+1) + "\r\n";

    int scores;
    switch(m_tasks.at(currentTask).getDifficulty())
    {
    case 1:
        scores = 2;
        break;
    case 2:
        scores = 3;
        break;
    case 3:
        scores = 5;
        break;
    }

    m_selectedAnswers[currentTask] = id;
    m_completenessBar->setValue(m_selectedAnswers.count());
    m_completenessBar->setFormat(QString("Ответов %1/%2").arg(m_completenessBar->value()).arg(TASK_COUNT));
    if (id == m_tasks.at(currentTask).getRightAnswer())
    {

        m_correctness[currentTask] = scores;
    }
    else
    {
        m_correctness[currentTask] = -1;
    }
}

void MainWindow::on_addTaskButton_clicked()
{
    TaskDialog *taskD = new TaskDialog();
    updateThemes();
    taskD->setParams(m_themes, this);
    taskD->show();
}

void MainWindow::setAllTasks(QList<Task> tasks)
{
    m_alltasks = tasks;
}

QList<Task> MainWindow::getAllTasks()
{
    return m_alltasks;
}

void MainWindow::setSecretChapterIsVisible()
{
    setPage(4);
    authorized = true;

    QFile file("lib/my.tests");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Невозможно открыть файл с тестами", file.errorString());
        return;
    }
    else
    {
        testsDom.setContent(decodeStr(QString(file.readAll())));
        file.close();
    }
    m_themes.clear();
    m_alltasks.clear();
    QString question;
    QList<QString> answers;
    int rightAnswer;
    QString difficulty;

    QFile file("lib/my.tests");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Невозможно открыть файл с тестами", file.errorString());
        return;
    }
    else
    {
        testsDom.setContent(decodeStr(QString(file.readAll())));
        file.close();
    }
    m_themes.clear();
    m_alltasks.clear();
    QString question;
    QList<QString> answers;
    int rightAnswer;
    QString difficulty;

    QDomNodeList themesList = testsDom.elementsByTagName("theme");
    QDomNode themeNode;
    QString theme;
    QDomNodeList partsList;
    for (int i = 0; i < themesList.count(); i++)
    {

       theme = themesList.at(i).toElement().attribute("name");
       m_themes.append(theme);
       partsList = themesList.at(i).toElement().elementsByTagName("part");

       for (int j = 0; j < partsList.count(); j++)
       {
           answers.clear();
           QDomElement elem = partsList.at(j).toElement();
           QDomNodeList qAa = elem.childNodes();
           question = qAa.at(0).toElement().text();
           for (int k = 1; k < qAa.count(); k++)//because first was answer
           {
               if (qAa.at(k).toElement().attribute("correct").contains("true"))
                   rightAnswer = k-1;
               answers.append(qAa.at(k).toElement().text());
           }
           difficulty = elem.attribute("difficulty");

           Task newTask(question, answers, rightAnswer, difficulty, theme);
           m_alltasks.append(newTask);
       }
    }
    confTextUpdate();
}

void MainWindow::confTextUpdate()
{
    QString str;

    for (int i = 0; i < m_alltasks.count(); i++)
    {
        str.append(QString("<h3>Вопрос №") + QString::number(i+1) + QString("</h3>"));
        QString diff;
        switch(m_alltasks.at(i).getDifficulty())
        {
            case 1:
                diff = "Легко";
                break;
            case 2:
                diff = "Средне";
                break;
            case 3:
                diff = "Сложно";
                break;
        }
        str.append(QString("<h3>Тема: ") +  m_alltasks.at(i).getTheme()
                   + QString("   Сложность: ") +  diff + QString("</h3>"));
        str.append(QString("<p><i>") + m_alltasks.at(i).getQuestion() + QString("</i></p>"));
        QList<QString> answers = m_alltasks.at(i).getAnswers();
        for (int i = 0; i < answers.count(); i++)
        {
            if(i == 0)
                str.append(QString("<p><font color=green>") + QString::number(i+1) + QString(". ") + answers.at(i) + QString("</font></p>"));
            else
                str.append(QString("<p>") + QString::number(i+1) + QString(". ") + answers.at(i) + QString("</p>"));
        }
    }

    ui->confTextBrowser->setText(str);
    ui->confTextBrowser->zoomIn(2);
}

void MainWindow::updateThemes()
{
    m_themes.clear();
    for (int i = 0; i < m_alltasks.count(); i++)
    {
        QString taskTheme = m_alltasks.at(i).getTheme();
        if (m_themes.indexOf(taskTheme) == -1)
            m_themes.append(taskTheme);
    }
}

void MainWindow::on_saveButton_clicked()
{   
    QDomDocument forSave("tests");
    QDomElement rootElem = forSave.createElement("tests");
    forSave.appendChild(rootElem);

    updateThemes();
    for (int i = 0; i < m_themes.count(); i++)
    {
        QDomElement themeElem = forSave.createElement("theme");
        themeElem.setAttribute("name", m_themes.at(i));
        rootElem.appendChild(themeElem);
    }

    QString diff;
    for (int i = 0; i < m_alltasks.count(); i++)
    {
        QDomElement partElem = forSave.createElement("part");
        switch(m_alltasks.at(i).getDifficulty())
        {
            case 1:
                diff = "easy";
                break;
            case 2:
                diff = "medium";
                break;
            case 3:
                diff = "difficult";
                break;
        }
        partElem.setAttribute("difficulty", diff);
        QDomElement questElem = forSave.createElement("question");
        QDomText questText = forSave.createTextNode(m_alltasks.at(i).getQuestion());
        questElem.appendChild(questText);
        partElem.appendChild(questElem);

        QList<QString> answers = m_alltasks.at(i).getAnswers();
        int rindex = m_alltasks.at(i).getRightAnswer();
        for(int a = 0; a < answers.count(); a++)
        {
            QDomElement ansElem = forSave.createElement("var");
            if (a == rindex)
            {
                ansElem.setAttribute("correct", "true");
            }
            else
            {
                ansElem.setAttribute("correct", "false");
            }
            QDomText ansText = forSave.createTextNode(answers.at(a));
            ansElem.appendChild(ansText);
            partElem.appendChild(ansElem);
        }

        QDomNodeList myThemes = forSave.elementsByTagName("theme");
        QDomElement currThemeElem;
        QString currTheme = m_alltasks.at(i).getTheme();
        for (int t = 0; t < myThemes.count(); t++)
        {
            if (myThemes.at(t).toElement().attribute("name").contains(currTheme))
            {
                currThemeElem = myThemes.at(t).toElement();
                currThemeElem.appendChild(partElem);
            }
        }

    }


    QFile file("./lib/my.tests");
    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, "Проблема сохранения", "Невозможно открыть файл с тестами", file.errorString());
        return;
    }
    else
    {
        QTextStream writeStream(&file);
        writeStream << encodeStr(forSave.toString());
        file.close();
        QMessageBox::information(this, "Информация", "Успешно сохранено");
        confTextUpdate();
    }

}

void MainWindow::on_editTaskButton_clicked()
{
    if(m_alltasks.count() < 1)
    {
      QMessageBox::information(this, "Информация", "Сначала создайте хотя бы один вопрос");
      return;
    }

    TaskDialog *taskD = new TaskDialog();
    updateThemes();
    taskD->isEdit = true;
    taskD->setParams(m_themes, this);
    taskD->show();
}

void MainWindow::on_deleteTaskButton_clicked()
{
    if(m_alltasks.count() < 1)
    {
      QMessageBox::information(this, "Информация", "Сначала создайте хотя бы один вопрос");
      return;
    }

    TaskDialog *taskD = new TaskDialog();
    updateThemes();
    taskD->isDelete = true;
    taskD->setParams(m_themes, this);
    taskD->show();
}

QString MainWindow::encodeStr(const QString& str)
{
    QByteArray arr(str.toUtf8());
    for(int i =0; i<arr.size(); i++)
        arr[i] = arr[i] ^ key;

    return QString::fromUtf8(arr.toBase64());
}

QString MainWindow::decodeStr(const QString &str)
{
    QByteArray arr = QByteArray::fromBase64(str.toUtf8());
    for(int i =0; i<arr.size(); i++)
        arr[i] =arr[i] ^ key;

    return QString::fromUtf8(arr);
}

//функция для 1 группы n
double f1(double a, double b, double x, double y, double z)
{
    return a * y + b * z;
}

//функция для с
double f2(double a, double b, double x, double y, double z)
{
    return a * y - b * z;
}

//функция для 6-и групп n
double f3(double a, double d1, double d2, double d3, double d4, double d5, double d6,
          double x, double y, double z1, double z2, double z3, double z4, double z5, double z6)
{
    return a * y + d1 * z1 + d2 * z2 + d3 * z3 + d4 * z4 + d5 * z5 + d6 * z6;
}

void MainWindow::on_pushButton_5_clicked()
{

    double p;
    QString S2 = ui->lineEdit_2->text();
    p = S2.toDouble();

    if (p > 1)
    {
        QMessageBox::warning(this,tr("Предупреждение"),
                             tr("Слишком большое по модулю значение реактивности"));
        return;
    }

    if (p < -1)
    {
        QMessageBox::warning(this,tr("Предупреждение"),
                             tr("Слишком большое по модулю значение реактивности"));
        return;
    }

    double lm;
    QString S3 = ui->lineEdit_3->text();
    lm = S3.toDouble();

    if (lm < 0)
    {
        QMessageBox::warning(this,tr("Ошибка"),
                             tr("Значение времени жизни мгновенных нейтронов отрицательное"));
        return;
    }

    if (lm > 0.1)
    {
        QMessageBox::warning(this,tr("Ошибка"),
                             tr("Значение времени жизни мгновенных нейтронов слишком большое"));
        return;
    }

    int N;
    QString S4 = ui->lineEdit_4->text();
    N = S4.toDouble();

    double h;
    QString S5 = ui->lineEdit_5->text();
    h = S5.toDouble();

    if ( N < 1)
    {
        QMessageBox::warning(this,tr("Предупреждение"),
                             tr("Неверное количество точек"));
        return;
    }

    if ( h < 0)
    {
        QMessageBox::warning(this,tr("Предупреждение"),
                             tr("Отрицательный шаг"));
        return;
    }

    if ( N < h)
    {
        QMessageBox::warning(this,tr("Предупреждение"),
                             tr("Шаг больше количества точек"));
        return;
    }

    if ( N/h > 100000)
    {
        QMessageBox::warning(this,tr("Предупреждение"),
                             tr("Задайте другой шаг или количество точек"));
        return;
    }

    double minx, maxx, miny, maxy;


    // При p > 0,p < beta значение n плавно возрастает.
    // При p > beta значение n резко возрастает.
    // При p < 0 значение n стремится к нулю.


    //double p = 0.008;// реактивность.
    //double lm = 0.001;//время жизни мгновенных нейтронов, с;
    double beta = 0.0064;//
    double lambda = 0.077;//постоянная распада, с^-1;
    double beta_i[6] = {0.00021, 0.00140, 0.00126, 0.00252, 0.00074, 0.00027};
    double lambda_i[6] = {0.0124, 0.0305, 0.1114, 0.3013, 1.136, 3.013};

    //const int N = 1000;//число точек на отрезке
    //double h = 0.01;//шаг//1.0E-2
    double Eps = 0.001;
    double k1, k2, k3, k4;
    double q1, q2, q3, q4;
    double time[N + 1], n[N + 1], c[N + 1], nnn[N+1];

    //начальные значения
    time[0] = 0;
    n[0] = 2.273E+8;//0.5//1000/2.273E+8
    c[0] = 85*n[0];//30//1.0E+6//84,416*n[0]//1.9185E+16//85*n[0]

    double a = (p-beta)/lm;
    double b = lambda;
    double g = beta/lm;
    double e = lambda;

    QVector<double> timett, timet, nn, cc, n_n_0;

    //ofstream outN("N_1.txt", ios_base::trunc);


    if(ui->radioButton->isChecked())
    {

        QString spin = ui->doubleSpinBox->text();
        QString spin2 = ui->doubleSpinBox_2->text();
        QString spin3 = ui->doubleSpinBox_3->text();
        QString spin4 = ui->doubleSpinBox_4->text();
        minx = ui->doubleSpinBox->value();
        maxx = ui->doubleSpinBox_2->value();
        miny = ui->doubleSpinBox_3->value();;
        maxy = ui->doubleSpinBox_4->value();

   /*     for (double tt = 0; tt < N; tt=tt+0.001)
        {
            nnn[tt+1]=(beta/(beta-p))*exp(lambda*p*tt/(beta-p))-(p/(beta-p))*exp(-((beta-p)/lm)*tt);
            n_n_0.append(nnn[tt+1]);
            timett.append(tt);
            ofstream outN1("N-1_group_D.txt", ios_base::app);
            outN1 << nnn[tt+1] << "\n";
        }
  */

        for (int t = 0; t < N; t++)
           {
               k1 = f1(a, b, time[t], n[t], c[t]);
               q1 = f2(g, e, time[t], n[t], c[t]);

               k2 = f1(a, b, time[t] + h / 2, n[t] + (h / 2)*k1, c[t] + (h / 2)*q1);
               q2 = f2(g, e, time[t] + h / 2, n[t] + (h / 2)*k1, c[t] + (h / 2)*q1);

               k3 = f1(a, b, time[t] + h / 2, n[t] + (h / 2)*k2, c[t] + (h / 2)*q2);
               q3 = f2(g, e, time[t] + h / 2, n[t] + (h / 2)*k2, c[t] + (h / 2)*q2);

               k4 = f1(a, b, time[t] + h, n[t] + h*k3, c[t] + h*q3);
               q4 = f2(g, e, time[t] + h, n[t] + h*k3, c[t] + h*q3);

               n[t + 1] = n[t] + (h / 6)*(k1 + 2 * k2 + 2 * k3 + k4);//плотность потока
               c[t + 1] = c[t] + (h / 6)*(q1 + 2 * q2 + 2 * q3 + q4);//концентрация предшественников
               time[t + 1] = time[t] + h;//время


               //запись значений в вектор: и t, и n, и c.
               timet.append(time[t]);
               nn.append(((n[t+1])/n[0]));// сразу нормируем
               cc.append(c[t + 1]);
               //qDebug() << timet;
               //qDebug() << nn;
               //qDebug() << cc;

               ofstream outN("N-1_group_RK.txt", ios_base::app);//данные по Рунге-Кутты
               outN << n[t+1] << "\n";

               //по формуле из Дементьева
               nnn[t+1]=(beta/(beta-p))*exp(lambda*p*time[t]/(beta-p))-(p/(beta-p))*exp(-((beta-p)/lm)*time[t]);
               n_n_0.append(nnn[t+1]);

               ofstream outN1("N-1_group_D.txt", ios_base::app);//данные по формуле из Дементьева
               outN1 << nnn[t+1] << "\n";

           }

       qDebug() << nn;
       qDebug() << "End";



       //Строим график
       //значение для построения графика должны быть в векторной форме

       ui->widget->clearGraphs();//Очищаем графики
       //Добавляем один график в widjet
       ui->widget->addGraph();
       //Говорим, что отрисовать нужно график по двум нашим массивам
       ui->widget->graph(0)->setData(timet, nn);//nn - для РК//n_n_0 -для формулы из Дементьева



       //Подписываем оси 0x и 0y
       ui->widget->xAxis->setLabel("time");
       ui->widget->yAxis->setLabel("n/n_0");

       //Установим область, которая будет показываться на графике
       ui->widget->xAxis->setRange(minx, maxx);//Для оси Ox
       ui->widget->yAxis->setRange(miny, maxy);//Для оси Oy

       //И перерисуем график на нашем widjet
       ui->widget->replot();//Чтобы любые изменения в графике появлялись на экране
    }

    //outN.close();
    //outN1.close();

    if(ui->radioButton_2->isChecked())
        {
            QMessageBox::information(this,"Title", "Not done");


           /* ui->doubleSpinBox->setValue(0);
            ui->doubleSpinBox_2->setValue(10);
            ui->doubleSpinBox_3->setValue(0);
            ui->doubleSpinBox_4->setValue(100);*/

            QString spin = ui->doubleSpinBox->text();
            QString spin2 = ui->doubleSpinBox_2->text();
            QString spin3 = ui->doubleSpinBox_3->text();
            QString spin4 = ui->doubleSpinBox_4->text();
            minx = ui->doubleSpinBox->value();
            maxx = ui->doubleSpinBox_2->value();
            miny = ui->doubleSpinBox_3->value();;
            maxy = ui->doubleSpinBox_4->value();

            double c[7][N + 1];
            double q11, q21, q31, q41;
            double q12, q22, q32, q42;
            double q13, q23, q33, q43;
            double q14, q24, q34, q44;
            double q15, q25, q35, q45;
            double q16, q26, q36, q46;

            double d[7];
            for (int m = 1; m < 7; m++)
            {
                d[m] = beta_i[m-1]/lm;
            }

            c[1][0] = 85*n[0];
            c[2][0] = 85*n[0];// начиная со второго, начальные значения для с_i
            c[3][0] = 85*n[0];// должны бать другими, так как концентрация осколков
            c[4][0] = 85*n[0];// деления увеличивается с увеличением групп
            c[5][0] = 85*n[0];
            c[6][0] = 85*n[0];

            for (int t = 0; t < N; t++)
               {
                   k1 = f3(a, lambda_i[0], lambda_i[1], lambda_i[2], lambda_i[3],
                              lambda_i[4], lambda_i[5], time[t], n[t],
                             c[1][t], c[2][t], c[3][t], c[4][t], c[5][t], c[6][t]);

                   q11 = f2(d[1], lambda_i[0], time[t], n[t], c[1][t]);
                   q12 = f2(d[2], lambda_i[1], time[t], n[t], c[2][t]);
                   q13 = f2(d[3], lambda_i[2], time[t], n[t], c[3][t]);
                   q14 = f2(d[4], lambda_i[3], time[t], n[t], c[4][t]);
                   q15 = f2(d[5], lambda_i[4], time[t], n[t], c[5][t]);
                   q16 = f2(d[6], lambda_i[5], time[t], n[t], c[6][t]);

                   k2 = f3(a, lambda_i[0], lambda_i[1], lambda_i[2], lambda_i[3],
                              lambda_i[4], lambda_i[5], time[t] + h / 2, n[t] + (h / 2)*k1,
                           c[1][t] + (h / 2)*q11, c[2][t] + (h / 2)*q12, c[3][t] + (h / 2)*q13,
                           c[4][t] + (h / 2)*q14, c[5][t] + (h / 2)*q15, c[6][t] + (h / 2)*q16);

                   q21 = f2(d[1], lambda_i[0], time[t] + h / 2, n[t] + (h / 2)*k1, c[1][t] + (h / 2)*q11);
                   q22 = f2(d[2], lambda_i[1], time[t] + h / 2, n[t] + (h / 2)*k1, c[2][t] + (h / 2)*q12);
                   q23 = f2(d[3], lambda_i[2], time[t] + h / 2, n[t] + (h / 2)*k1, c[3][t] + (h / 2)*q13);
                   q24 = f2(d[4], lambda_i[3], time[t] + h / 2, n[t] + (h / 2)*k1, c[4][t] + (h / 2)*q14);
                   q25 = f2(d[5], lambda_i[4], time[t] + h / 2, n[t] + (h / 2)*k1, c[5][t] + (h / 2)*q15);
                   q26 = f2(d[6], lambda_i[5], time[t] + h / 2, n[t] + (h / 2)*k1, c[6][t] + (h / 2)*q16);

                   k3 = f3(a, lambda_i[0], lambda_i[1], lambda_i[2], lambda_i[3],
                              lambda_i[4], lambda_i[5], time[t] + h / 2, n[t] + (h / 2)*k2,
                           c[1][t] + (h / 2)*q21, c[2][t] + (h / 2)*q22, c[3][t] + (h / 2)*q23,
                           c[4][t] + (h / 2)*q24, c[5][t] + (h / 2)*q25, c[6][t] + (h / 2)*q26);

                   q31 = f2(d[1], lambda_i[0], time[t] + h / 2, n[t] + (h / 2)*k2, c[1][t] + (h / 2)*q21);
                   q32 = f2(d[2], lambda_i[1], time[t] + h / 2, n[t] + (h / 2)*k2, c[2][t] + (h / 2)*q22);
                   q33 = f2(d[3], lambda_i[2], time[t] + h / 2, n[t] + (h / 2)*k2, c[3][t] + (h / 2)*q23);
                   q34 = f2(d[4], lambda_i[3], time[t] + h / 2, n[t] + (h / 2)*k2, c[4][t] + (h / 2)*q24);
                   q35 = f2(d[5], lambda_i[4], time[t] + h / 2, n[t] + (h / 2)*k2, c[5][t] + (h / 2)*q25);
                   q36 = f2(d[6], lambda_i[5], time[t] + h / 2, n[t] + (h / 2)*k2, c[6][t] + (h / 2)*q26);

                   k4 = f3(a, lambda_i[0], lambda_i[1], lambda_i[2], lambda_i[3],
                              lambda_i[4], lambda_i[5], time[t] + h, n[t] + h*k3,
                              c[1][t] + h*q31, c[2][t] + h*q32, c[3][t] + h*q33,
                              c[4][t] + h*q34, c[5][t] + h*q35, c[6][t] + h*q36);

                   q41 = f2(d[1], lambda_i[0], time[t] + h, n[t] + h*k3, c[1][t] + h*q31);
                   q42 = f2(d[2], lambda_i[1], time[t] + h, n[t] + h*k3, c[2][t] + h*q32);
                   q43 = f2(d[3], lambda_i[2], time[t] + h, n[t] + h*k3, c[3][t] + h*q33);
                   q44 = f2(d[4], lambda_i[3], time[t] + h, n[t] + h*k3, c[4][t] + h*q34);
                   q45 = f2(d[5], lambda_i[4], time[t] + h, n[t] + h*k3, c[5][t] + h*q35);
                   q46 = f2(d[6], lambda_i[5], time[t] + h, n[t] + h*k3, c[6][t] + h*q36);

                   n[t + 1] = n[t] + (h / 6)*(k1 + 2 * k2 + 2 * k3 + k4);//плотность потока
                   c[1][t + 1] = c[1][t] + (h / 6)*(q11 + 2 * q21 + 2 * q31 + q41);//концентрация предшественников
                   c[2][t + 1] = c[2][t] + (h / 6)*(q12 + 2 * q22 + 2 * q32 + q42);//концентрация предшественников
                   c[3][t + 1] = c[3][t] + (h / 6)*(q13 + 2 * q23 + 2 * q33 + q43);//концентрация предшественников
                   c[4][t + 1] = c[4][t] + (h / 6)*(q14 + 2 * q24 + 2 * q34 + q44);//концентрация предшественников
                   c[5][t + 1] = c[5][t] + (h / 6)*(q15 + 2 * q25 + 2 * q35 + q45);//концентрация предшественников
                   c[6][t + 1] = c[6][t] + (h / 6)*(q16 + 2 * q26 + 2 * q36 + q46);//концентрация предшественников
                   time[t + 1] = time[t] + h;//время


                   //запись значений в вектор: и t, и n, и c.
                   timet.append(time[t]);
                   nn.append((n[t+1])/n[0]);// сразу нормируем
                   //cc.append(c[t + 1]);
                   //qDebug() << timet;
                   //qDebug() << nn;
                   //qDebug() << cc;

                   ofstream outN("N-6_group.txt", ios_base::app);
                   outN << n[t+1] << "\n";

               }

           qDebug() << nn;
           qDebug() << "End";
           //outN.close();

           //Строим график
           //значение для построения графика должны быть в векторной форме

           ui->widget->clearGraphs();//Очищаем графики
           //Добавляем один график в widjet
           ui->widget->addGraph();
           //Говорим, что отрисовать нужно график по двум нашим массивам
           ui->widget->graph(0)->setData(timet, nn);//nn



           //Подписываем оси 0x и 0y
           ui->widget->xAxis->setLabel("time");
           ui->widget->yAxis->setLabel("n/n_0");

           //Установим область, которая будет показываться на графике
           ui->widget->xAxis->setRange(minx, maxx);//Для оси Ox,
           ui->widget->yAxis->setRange(miny, maxy);//Для оси Oy,

           //И перерисуем график на нашем widjet
           ui->widget->replot();//Чтобы любые изменения в графике появлялись на экране
        }
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
}




