#include "mainwindow.h"
#include "taskdialog.h"
#include <QApplication>
#include <QTextCodec>

int main(int argc, char *argv[])
{
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    QApplication a(argc, argv);
    MainWindow w;    
    //w.setFixedSize(w.size());
    w.setWindowIcon(QIcon(":/icons/main.png"));
    w.setStyleSheet("QMainWindow {background: 'lightgrey';}");

   // w.showFullScreen();
    w.show();
    return a.exec();
}
