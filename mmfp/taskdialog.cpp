#include "taskdialog.h"
#include "ui_taskdialog.h"
#include "task.h"
#include <QMessageBox>
#include <QMainWindow>
#include <QDebug>

TaskDialog::TaskDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TaskDialog)
{
    ui->setupUi(this);
    ui->themelineEdit->hide();

}

TaskDialog::~TaskDialog()
{
    delete ui;
}

void TaskDialog::setParams(QList<QString> themes, MainWindow *_main)
{
    m_themes = themes;
    m_main = _main;
    m_tasks = m_main->getAllTasks();    

    if(isEdit)
    {
       this->setWindowTitle("Редактирование вопроса");
       ui->tasknLabel->show();
       ui->comboBox->show();
       ui->addButton->setText("Редактировать вопрос");
       displayTask(0);
    }
    else if (isDelete)
    {
        this->setWindowTitle("Удаление вопроса");
        ui->tasknLabel->show();
        ui->comboBox->show();
        ui->addButton->setText("Удалить вопрос");
        ui->ans1Text->setDisabled(true);
        ui->ans2Text->setDisabled(true);
        ui->ans3Text->setDisabled(true);
        ui->ans4Text->setDisabled(true);
        ui->ans5Text->setDisabled(true);
        ui->themecomboBox->setDisabled(true);
        ui->questplainTextEdit->setDisabled(true);
        ui->diffcomboBox->setDisabled(true);
        displayTask(0);
    }
    else
    {
       this->setWindowTitle("Добавление вопроса");
       ui->tasknLabel->hide();
       ui->comboBox->hide();
    }

    if (isEdit || isDelete)
    {
        for(int i = 0; i < m_tasks.count(); i++)
            ui->comboBox->addItem(QString("Вопрос %1").arg(i+1));
    }

    ui->themecomboBox->addItems(m_themes);
    ui->themecomboBox->addItem("Добавить новую тему...");
    ui->diffcomboBox->addItem("Легко");
    ui->diffcomboBox->addItem("Средне");
    ui->diffcomboBox->addItem("Сложно");
}

void TaskDialog::on_addButton_clicked()
{    
    QString question;
    QList<QString> answers;
    int rightAnswer;
    QString difficulty;
    QString theme;

    if(ui->questplainTextEdit->toPlainText().isEmpty())
    {
        QMessageBox::warning(0, "Информация", "Заполните текст вопроса");
        return;
    }
    else
    {
        question = ui->questplainTextEdit->toPlainText();
    }

    if(ui->ans1Text->toPlainText().isEmpty())
    {
        QMessageBox::warning(0, "Информация", "Введите правильный ответ");
        return;
    }

    if(!ui->ans1Text->toPlainText().isEmpty())
        answers.append(ui->ans1Text->toPlainText());
    if(!ui->ans2Text->toPlainText().isEmpty())
        answers.append(ui->ans2Text->toPlainText());
    if(!ui->ans3Text->toPlainText().isEmpty())
        answers.append(ui->ans3Text->toPlainText());
    if(!ui->ans4Text->toPlainText().isEmpty())
        answers.append(ui->ans4Text->toPlainText());
    if(!ui->ans5Text->toPlainText().isEmpty())
        answers.append(ui->ans5Text->toPlainText());

    if(ui->radioButton->isChecked())
        rightAnswer = 0;

    if(ui->themecomboBox->currentText().contains("Добавить новую тему..."))
        theme = ui->themelineEdit->text();
    else
        theme = ui->themecomboBox->currentText();

    difficulty = ui->diffcomboBox->currentText();
    if(difficulty.contains("Легко"))
        difficulty = "easy";
    else if(difficulty.contains("Средне"))
        difficulty = "medium";
    else if(difficulty.contains("Сложно"))
        difficulty = "difficult";

    if(answers.count() < 2)
    {
        QMessageBox::information(0, "Информация", "Минимальное количество ответов - 2");
        return;
    }


    Task newTask(question, answers, rightAnswer, difficulty, theme);
    if(isEdit)
       m_tasks.replace(ui->comboBox->currentIndex(), newTask);
    else if (isDelete)
    {
        m_tasks.removeAt(ui->comboBox->currentIndex());
    }
    else
        m_tasks.append(newTask);

    m_main->setAllTasks(m_tasks);
    m_main->confTextUpdate();
    this->close();
}

void TaskDialog::on_themecomboBox_currentTextChanged(const QString &arg1)
{
    if (arg1.contains("Добавить новую тему..."))
        ui->themelineEdit->show();
    else
        ui->themelineEdit->hide();

}

void TaskDialog::displayTask(int index)
{
    ui->themecomboBox->setCurrentText(m_tasks.at(index).getTheme());
    switch(m_tasks.at(index).getDifficulty())
    {
        case 1:
            ui->diffcomboBox->setCurrentText("Легко");
            break;
        case 2:
            ui->diffcomboBox->setCurrentText("Средне");
            break;
        case 3:
            ui->diffcomboBox->setCurrentText("Сложно");
            break;
    }
    ui->questplainTextEdit->setPlainText(m_tasks.at(index).getQuestion());

    QList<QString> ans = m_tasks.at(index).getAnswers();
    int r = m_tasks.at(index).getRightAnswer();

    for (int i = 0; i < ans.count(); i++)
    {
         switch(i)
        {
        case 0:
            ui->ans1Text->setPlainText(ans.at(i));
            break;
        case 1:
            ui->ans2Text->setPlainText(ans.at(i));
            break;
        case 2:
            ui->ans3Text->setPlainText(ans.at(i));
            break;
        case 3:
            ui->ans4Text->setPlainText(ans.at(i));
            break;
        case 4:
             ui->ans5Text->setPlainText(ans.at(i));
             break;
        }
    }

}

void TaskDialog::on_comboBox_currentIndexChanged(int index)
{
    ui->questplainTextEdit->clear();
    ui->ans1Text->clear();
    ui->ans2Text->clear();
    ui->ans3Text->clear();
    ui->ans4Text->clear();
    ui->ans5Text->clear();
    displayTask(index);
}
