#include "resultsdialog.h"
#include "ui_resultsdialog.h"
#include <QDebug>

ResultsDialog::ResultsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ResultsDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Результаты");
}

ResultsDialog::~ResultsDialog()
{
    delete ui;
}

void ResultsDialog::setParams(QString studentName, QMap<int, int> correctness, QMap<int, int> selectedAnswers, QList<Task> tasks)
{
    m_studentName = studentName;
    m_correctness = correctness;
    m_selectedAnswers = selectedAnswers;
    m_tasks = tasks;

    QList<int> values = m_correctness.values();
    for(int i = 0; i < values.count(); i++)
    {
        m_score += values.at(i);
    }
    qDebug() << values.count();
    qDebug() << m_score;
    if(m_score >= 25)
        m_mark = 5;
    else if (m_score >= 20)
        m_mark = 4;
    else if (m_score >= 17)
        m_mark = 3;
    else
        m_mark = 2;
    qDebug() << m_mark;
    updateText();
}

void ResultsDialog::updateText()
{
    QString str;
    str.append(QString("<center><h3>Студент: ") + m_studentName + QString("</h2></center>"));
    str.append(QString("<center><h2>  ") + QString("</h2></center>"));
    str.append(QString("<center><h2>Оценка: ") + QString("%1").arg(m_mark) + QString("</h2></center>"));
    str.append(QString("<center><h3>Количество баллов: ") + QString("%1").arg(m_score) + QString("</h3></center>"));    
    str.append(QString("<h3>Ошибки ")  + QString("</h3>"));

    QList<int> values = m_correctness.values();
    for (int i = 0; i < values.count(); i++)
    {
        if (values.at(i) == -1)
        {
            QString diff;
            switch(m_tasks.at(i).getDifficulty())
            {
                case 1:
                    diff = "Легко";
                    break;
                case 2:
                    diff = "Средне";
                    break;
                case 3:
                    diff = "Сложно";
                    break;
            }

            str.append(QString("<h2>Вопрос №") + QString::number(i+1) + QString("</h2>"));
            str.append(QString("<h3>Тема: ") +  m_tasks.at(i).getTheme()
                       + QString("   Сложность: ") +  diff + QString("</h3>"));
            str.append(QString("<p><i>") + m_tasks.at(i).getQuestion() + QString("</i></p>"));
            QList<QString> answers = m_tasks.at(i).getAnswers();
             for (int j = 0; j < answers.count(); j++)
             {
                 if (j == m_tasks.at(i).getRightAnswer())
                 {
                      str.append(QString("<p><font color=green>") + QString::number(j+1) + QString(". ") + answers.at(j) + QString(" (верный)</font></p>"));
                 }
                 else if (j == m_selectedAnswers.values().at(i))
                 {
                     str.append(QString("<p><font color=red>") + QString::number(j+1) + QString(". ") + answers.at(j) + QString("</font></p>"));
                 }
                 else
                 {
                     str.append(QString("<p>") + QString::number(j+1) + QString(". ") + answers.at(j) + QString("</p>"));
                 }

             }
        }
    }



    ui->textBrowser->setHtml(str);
}
