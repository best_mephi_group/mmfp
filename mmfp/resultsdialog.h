#ifndef RESULTSDIALOG_H
#define RESULTSDIALOG_H

#include <QDialog>
#include "task.h"
#include <QMap>

namespace Ui {
class ResultsDialog;
}

class ResultsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ResultsDialog(QWidget *parent = 0);
    ~ResultsDialog();

    void setParams(QString studentName, QMap<int, int> correctness, QMap<int, int> selectedAnswers, QList<Task> tasks);

private:
    Ui::ResultsDialog *ui;
    QString m_studentName;
    QMap<int, int> m_correctness;
    QMap<int, int> m_selectedAnswers;
    QList<Task> m_tasks;

    int m_mark;
    int m_score = 0;

    void updateText();
};

#endif // RESULTSDIALOG_H
